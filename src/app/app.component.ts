import { Component } from '@angular/core';
import {
  CdkDragDrop, 
  CdkDragSortEvent, 
  CdkDragStart,
  moveItemInArray, 
  transferArrayItem
} from '@angular/cdk/drag-drop';
import { trigger, state, style, animate, transition } from '@angular/animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('initList', [
      transition(':enter', [
        style({ 
          transform: 'scale(0.2)', 
          opacity: 0 
        }),
        animate('1s cubic-bezier(.8, -0.6, 0.2, 1.5)', 
        style({ 
          transform: 'scale(1)', 
          opacity: 1 
        }))
      ])
    ]),
    trigger('dragDrop', [
      state('initial', style({
        opacity: 1,
        color: '#000000de',
        backgroundColor: 'white'
      })),
      state('drag', style({
        opacity: 0.5,
        color: '#000000de',
        backgroundColor: 'yellow'
      })),
      transition('* => drag', [
        animate('600ms')
      ]),
      transition('* => initial', [
        animate('600ms')
      ])
    ])
  ]
})
export class AppComponent {
  title = 'assignment';
  state = "initial";
  todo = [
    'Get to work',
    'Pick up groceries',
    'Go home',
    'Fall asleep'
  ];

  done = [
    'Get up',
    'Brush teeth',
    'Take a shower',
    'Check e-mail',
    'Walk dog'
  ];

  drop(event: CdkDragDrop<string[]>) {
    this.state = 'initial'; 
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);
    }
  }

  startDragging(event){
    this.state = 'drag';
  }
}
